function undisplay(element) {
  element.style.display="none";
}

function add_message(severity, message) {
  message = `<div class="message ${severity}" onclick="undisplay(this)">
<span class="hide-button" onclick="undisplay(this.parentElement)">&times;</span>
${message}
</div>`;
  const message_area = document.getElementById('message-area');
  message_area.insertAdjacentHTML('beforeend', message);
}

function lmno_join_loadend(request, game_id) {
  if (request.status === 404) {
    add_message("danger", game_id + " is not a valid game ID. Try again.");
    return;
  }

  /* Now that its validated, send the browser to the URL for the game_id. */
  window.location.href = "/" + game_id;
}

function lmno_join(form) {
  const game_id = form.id.value;

  var request = new XMLHttpRequest();
  request.addEventListener("loadend", () => lmno_join_loadend(request, game_id));

  request.open("GET", "/" + game_id);
  request.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
  request.send();

  form.reset();
}

function lmno_new_loadend() {
  if (this.status == 200) {
    /* Response contains the game ID which we simply point the browser to. */
    const game_id = JSON.parse(this.response);
    window.location.href = ('/' + game_id);
    return;
  }

  add_message("danger", `An error occured creating a new game (${this.status}).`);
}

function lmno_new(engine) {
  const request = new XMLHttpRequest();
  request.addEventListener("loadend", lmno_new_loadend);

  request.open("POST", "/new/" + engine);
  request.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
  request.send();

  return false;
}
