DEPLOY_HOST=lmno.games
DEPLOY_DIR=/srv/lmno.games/www
DO_NOT_DEPLOY=Makefile deps *.jsx .babelrc .gitignore README
DO_NOT_DELETE=flempires

REACT_DEPS=react.js react-dom.js
REACT_DOWNLOADS=\
  deps/react.development.js \
  deps/react-dom.development.js \
  deps/react.production.min.js \
  deps/react-dom.production.min.js

JSX_SOURCE=$(wildcard */*.jsx)
JS_TARGETS=$(JSX_SOURCE:.jsx=.js)

all: $(JS_TARGETS) $(REACT_DEPS)

# Execute either of the following to build things:
#
#    For a development build:
#
#	make
#
#    For a production build:
#
#	make LMNO_BUILD=production
#
# Note: To switch between these two, either issue a "make clean"
#       or otherwise start in a clean source tree (such as a
#       after a fresh "git clone" or use "git clean -f -x -d).
#
CP=cp
BABEL=babeljs
ifeq ($(LMNO_BUILD),production)
react.js: deps/react.production.min.js
	$(call quiet,CP) $^ $@

react-dom.js: deps/react-dom.production.min.js
	$(call quiet,CP) $^ $@

export BABEL_ENV=production
%.js: %.jsx
	$(call quiet,BABEL) $^ --out-file $@
	@chmod a-w $@
else
react.js: deps/react.development.js
	$(call quiet,CP) $^ $@

react-dom.js: deps/react-dom.development.js
	$(call quiet,CP) $^ $@

export BABEL_ENV=development
%.js: %.jsx
	$(call quiet,BABEL) $^ --out-file $@
	@chmod a-w $@
endif

# The user has not set any verbosity, default to quiet mode and inform the
# user how to enable verbose compiles.
ifeq ($(V),)
quiet_DOC := "Use \"$(MAKE) V=1\" to see the verbose compile lines.\n"
quiet = @printf $(quiet_DOC)$(eval quiet_DOC:=)"$(1) $(or $(2),$@)\n"; $($(word 1, $(1)))
WGET_VERBOSE_FLAGS=--quiet
endif
# The user has explicitly enabled quiet compilation.
ifeq ($(V),0)
quiet = @printf "$(1) $(or $(2),$@)\n"; $($(word 1, $(1)))
WGET_VERBOSE_FLAGS=--quiet
endif
# Otherwise, print the full command line.
quiet ?= $($(word 1, $(1)))
WGET_VERBOSE_FLAGS ?= --no-verbose

SHA512=sha512sum --strict -c
.PHONY: deps
deps: $(REACT_DOWNLOADS)
	$(call quiet,SHA512) deps/*.sha512

DOWNLOAD=wget $(WGET_VERBOSE_FLAGS) -nc -P deps
deps/%.js:
	$(call quiet,DOWNLOAD) https://unpkg.com/react@16/umd/$@
	$(call quiet,SHA512) $(patsubst %,%.sha512,$@)

deploy:
	rm -rf .deploy-source
	git clone . .deploy-source
	cp deps/*.js .deploy-source/deps
	make -C .deploy-source deps
	make -C .deploy-source LMNO_BUILD=production
	rm -rf .deploy-source/.git
	(cd .deploy-source; rsync -avz \
		$(DO_NOT_DEPLOY:%=--exclude=%) \
		--exclude=$(DO_NOT_DELETE) \
		--delete \
		--delete-after \
		./ $(DEPLOY_HOST):$(DEPLOY_DIR) )
	rm -rf .deploy-source

clean:
	rm -f $(JS_TARGETS)
	rm -f $(REACT_DEPS)
	rm -f deps/*.js
