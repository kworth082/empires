import 'dart:convert';

import 'package:http/http.dart' as http;

class Player {
  final int id;
  final String name;

  Player({this.id, this.name});

  factory Player.fromJson(Map<String, dynamic> json) {
    return Player(
      id: json['id'],
      name: json['name'],
    );
  }

  static Future<Player> fetchFirstPlayer() async {
    final response = await http.get('https://families.cworth.org/api/players');

    if (response.statusCode == 200) {
      List<Player> playerList = parsePlayers(response.body);
      return playerList.elementAt(0);
    } else {
      throw Exception('Failed to load player');
    }
  }

  static Future<List<Player>> fetchAllPlayers() async {
    final response = await http.get('https://families.cworth.org/api/players');

    if (response.statusCode == 200) {
      return parsePlayers(response.body);
    } else {
      throw Exception('Failed to load players');
    }
  }

  static List<Player> parsePlayers(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return parsed.map<Player>((json) => Player.fromJson(json)).toList();
  }
}
