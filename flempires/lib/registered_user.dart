class RegisteredUser {
  final String name;
  final String character;

  RegisteredUser({this.name, this.character});

  factory RegisteredUser.fromJson(Map<String, dynamic> json) {
    return RegisteredUser(
      name: json['name'],
      character: json['character'],
    );
  }
}
